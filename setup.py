import setuptools
import pyrkepac

setuptools.setup(
    name="pyrkepac",
    description="Test my beautiful CI",
    version=pyrkepac.__version__,
    packages=setuptools.find_packages()
)
